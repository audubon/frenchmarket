extern crate riker;
use riker::actors::*;

use clap::clap_app;

#[macro_use]
extern crate log;

use std::time::Duration;
use steel_cent::currency::USD;
use steel_cent::Money;

mod actors;
mod configuration;
mod marketdata;
mod messages;

// c:\work\audubon\frenchmarket>cargo run -- --start --config c:/work/audubon/frenchmarket/config/application.json

fn main() {
    let matches = clap_app!(myapp =>
        (version: "1.0")
        (author: "A <a@a.com>")
        (about: "Does awesome things")
        (@arg START: -s --start "Start")
        (@arg CONFIG: -c --config +takes_value "Sets a custom config file")
    )
    .get_matches();

    let cfg = matches.value_of("CONFIG");
    println!("using configuration {}", cfg.unwrap());
    let c = configuration::read_config_file(cfg.unwrap());
    println!("{:#?}", c.unwrap());

    let mut bbo = marketdata::top_of_book();
    bbo.insert(
        "IBM".to_string(),
        messages::Quote::create(
            Money::of_major_minor(USD, 255, 32),
            Money::of_major_minor(USD, 255, 44),
            String::from("MSFT"),
        ),
    );

    for (key, value) in &bbo {
        println!("{}: {}", key, value);
    }

    if matches.is_present("START") {
        runner();
    }
}

fn runner() {
    let sys = ActorSystem::new().unwrap();

    let myval = sys.config().get_str("app.myval").unwrap();
    println!("myval {}",myval);

    let quote_channel: ChannelRef<messages::Quote> = channel("quote-channel", &sys).unwrap();

    let strategy_actor = sys
        .actor_of_args::<actors::StrategyActor, _>("StrategyActor", quote_channel.clone())
        .unwrap();

    let _datafeed_actor = sys
        .actor_of_args::<actors::DataFeedActor, _>("DataFeedActor", quote_channel.clone())
        .unwrap();
        
    for i in 1..12 {
        strategy_actor.tell(
            messages::Quote::create(
                Money::of_major_minor(USD, 15, i),
                Money::of_major_minor(USD, 15, i+2),
                String::from("IBM"),
            ),
            None,
        );
    }

    let _sysactor = sys
        .actor_of::<actors::SystemActor>("SystemActor")
        .unwrap();

    let actor = sys
        .actor_of::<actors::ExecutionGateway>("ExecutionGateway")
        .unwrap();
    for _ in 1..8 {
        actor.tell(
            messages::Order::create(Money::of_major_minor(USD, 15, 96), String::from("IBM"), 500),
            None,
        );
    }

    actor.tell(
        messages::Fill::create(
            Money::of_major_minor(USD, 15, 96),
            String::from("IBM"),
            500,
            122,
        ),
        None,
    );

    info!("Creating panic actor");
    let panic_actor = sys.actor_of::<actors::PanicActor>("panic-actor").unwrap();
    info!("Send Panic message to dump actor to get restart");
    panic_actor.tell(messages::Panic, None);
    sys.print_tree();

    info!("stopping actor");
    sys.stop(&actor);

    std::thread::sleep(Duration::from_millis(5000));
    sys.print_tree();
}
