extern crate riker;
use riker::actors::*;

use super::messages::{Fill, Order, Panic, Quote};
use std::time::Duration;
use steel_cent::currency::USD;
use steel_cent::Money;

pub struct DataFeedActor {
    quote_channel: ChannelRef<Quote>,
}
impl ActorFactoryArgs<ChannelRef<Quote>> for DataFeedActor {
    fn create_args(quote_channel: ChannelRef<Quote>) -> Self {
        Self { quote_channel }
    }
}
impl Actor for DataFeedActor {
    type Msg = ();

    fn pre_start(&mut self, ctx: &Context<Self::Msg>) {
        info!("{}: -> prestart: ", ctx.myself.name());

        let q = super::messages::Quote::create(
            Money::of_major_minor(USD, 325, 84),
            Money::of_major_minor(USD, 326, 02),
            String::from("TSLA"),
        );

        info!("{}: -> publishing quote to channel: ", ctx.myself.name());
        self.quote_channel.tell(
            Publish {
                msg: q,
                topic: "power".into(),
            },
            None,
        );

        // schedule quotes
        let delay = Duration::from_millis(100);
        let iterv = Duration::from_millis(500);

        ctx.schedule(delay,
            iterv,
            self.quote_channel.clone(),
            None,
            Publish {
                msg: super::messages::Quote::create(
                    Money::of_major_minor(USD, 325, 84),
                    Money::of_major_minor(USD, 326, 02),
                    String::from("TSLA"),
                ),
                topic: "power".into(),
            });
    }
    fn recv(&mut self, ctx: &Context<Self::Msg>, msg: Self::Msg, _sender: Sender) {
        info!("{}: -> {:#?}", ctx.myself.name(), msg);
    }
}

#[actor(Quote)]
pub struct StrategyActor {
    quote_channel: ChannelRef<Quote>,
}

impl ActorFactoryArgs<ChannelRef<Quote>> for StrategyActor {
    fn create_args(quote_channel: ChannelRef<Quote>) -> Self {
        Self { quote_channel }
    }
}
impl Actor for StrategyActor {
    type Msg = StrategyActorMsg;

    fn pre_start(&mut self, ctx: &Context<Self::Msg>) {
        info!("{}: -> prestart: ", ctx.myself.name());

        let sub = Box::new(ctx.myself());
        self.quote_channel.tell(
            Subscribe {
                actor: sub,
                topic: "*".into(),
            },
            None,
        );
    }
    fn recv(&mut self, ctx: &Context<Self::Msg>, msg: Self::Msg, sender: Sender) {
        self.receive(ctx, msg, sender);
    }
}

impl Receive<Quote> for StrategyActor {
    type Msg = StrategyActorMsg;

    fn receive(&mut self, ctx: &Context<Self::Msg>, msg: Quote, _sender: Sender) {
        info!("{}: -> recived quote {}", ctx.myself.name(), msg);
    }
}

#[actor(Panic)]
#[derive(Default)]
pub struct PanicActor;

impl Actor for PanicActor {
    type Msg = PanicActorMsg;

    fn pre_start(&mut self, ctx: &Context<Self::Msg>) {
        info!("{}: -> prestart: ", ctx.myself.name());
        /*
        ctx.actor_of::<StrategyActor>("child_a").unwrap();

        ctx.actor_of::<StrategyActor>("child_b").unwrap();

        ctx.actor_of::<StrategyActor>("child_c").unwrap();

        ctx.actor_of::<StrategyActor>("child_d").unwrap();
        */
    }

    fn recv(&mut self, ctx: &Context<Self::Msg>, msg: Self::Msg, sender: Sender) {
        self.receive(ctx, msg, sender);
    }
}

impl Receive<Panic> for PanicActor {
    type Msg = PanicActorMsg;

    fn receive(&mut self, _ctx: &Context<Self::Msg>, _msg: Panic, _sender: Sender) {
        panic!("// TEST PANIC // TEST PANIC // TEST PANIC //");
    }
}

#[actor(Fill, Order)]
#[derive(Default)]
pub struct ExecutionGateway;

impl Actor for ExecutionGateway {
    // we used the #[actor] attribute so CounterMsg is the Msg type
    type Msg = ExecutionGatewayMsg;

    fn pre_start(&mut self, ctx: &Context<Self::Msg>) {
        info!("{}: -> prestart: ", ctx.myself.name());
        /*
        ctx.actor_of::<StrategyActor>("child_a").unwrap();

        ctx.actor_of::<StrategyActor>("child_b").unwrap();

        ctx.actor_of::<StrategyActor>("child_c").unwrap();

        ctx.actor_of::<StrategyActor>("child_d").unwrap();

        ctx.actor_of::<PanicActor>("child_panic").unwrap();
        */
    }

    fn post_start(&mut self, _ctx: &Context<Self::Msg>) {
        debug!("Counter post_start");
    }
    fn post_stop(&mut self) {
        debug!("Counter post_stop");
    }

    fn recv(&mut self, ctx: &Context<Self::Msg>, msg: Self::Msg, sender: Sender) {
        // Use the respective Receive<T> implementation
        self.receive(ctx, msg, sender);
    }
}

impl Receive<Order> for ExecutionGateway {
    type Msg = ExecutionGatewayMsg;

    fn receive(&mut self, ctx: &Context<Self::Msg>, msg: Order, _sender: Sender) {
        info!("{} -> ORDER: {}", ctx.myself.name(), msg);
    }
}

impl Receive<Fill> for ExecutionGateway {
    type Msg = ExecutionGatewayMsg;

    fn receive(&mut self, ctx: &Context<Self::Msg>, msg: Fill, _sender: Sender) {
        info!("{} -> FILL: {}", ctx.myself.name(), msg);
    }
}

#[actor(SystemEvent)]
#[derive(Default)]
pub struct SystemActor;

impl Actor for SystemActor {
    type Msg = SystemActorMsg;

    fn pre_start(&mut self, ctx: &Context<Self::Msg>) {
        let topic = Topic::from("*");

        info!(
            "{}: pre_start subscribe to topic {:?}",
            ctx.myself.name(),
            topic
        );
        let sub = Box::new(ctx.myself());

        ctx.system.sys_events().tell(
            Subscribe {
                actor: sub,
                topic: "*".into(),
            },
            None,
        );
    }

    fn recv(&mut self, ctx: &Context<Self::Msg>, msg: Self::Msg, sender: Sender) {
        self.receive(ctx, msg, sender);
    }

    fn sys_recv(&mut self, ctx: &Context<Self::Msg>, msg: SystemMsg, sender: Sender) {
        if let SystemMsg::Event(evt) = msg {
            self.receive(ctx, evt, sender);
        }
    }
}

impl Receive<SystemEvent> for SystemActor {
    type Msg = SystemActorMsg;

    fn receive(&mut self, ctx: &Context<Self::Msg>, msg: SystemEvent, _sender: Sender) {
        info!("{}: -> got system msg: {:?} ", ctx.myself.name(), msg);
        match msg {
            SystemEvent::ActorCreated(created) => {
                info!("path: {}", created.actor.path());
            }
            SystemEvent::ActorRestarted(restarted) => {
                info!("path: {}", restarted.actor.path());
            }
            SystemEvent::ActorTerminated(terminated) => {
                info!("path: {}", terminated.actor.path());
            }
        }
    }
}
