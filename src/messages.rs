use std::sync::atomic::{AtomicUsize, Ordering};
use std::time::Duration;
use std::time::{SystemTime, UNIX_EPOCH};
pub use steel_cent::Money;

fn get_orderid() -> usize {
    static COUNTER: AtomicUsize = AtomicUsize::new(1);
    COUNTER.fetch_add(1, Ordering::Relaxed)
}

fn get_fillid() -> usize {
    static COUNTER: AtomicUsize = AtomicUsize::new(1);
    COUNTER.fetch_add(1, Ordering::Relaxed)
}

// Define the messages we'll use
#[derive(Clone, Debug)]
pub struct Order {
    pub price: Money,
    pub symbol: String,
    pub quantity: i64,
    pub timestamp: Duration,
    pub orderid: usize,
}

impl Order {
    pub fn create(price: Money, symbol: String, quantity: i64) -> Order {
        Order {
            price,
            symbol,
            quantity,
            timestamp: SystemTime::now().duration_since(UNIX_EPOCH).unwrap(),
            orderid: get_orderid(),
        }
    }
}

#[derive(Clone, Debug)]
pub struct Fill {
    pub price: Money,
    pub symbol: String,
    pub quantity: i64,
    pub timestamp: Duration,
    pub orderid: usize,
    pub fillid: usize,
}

impl Fill {
    pub fn create(price: Money, symbol: String, quantity: i64, orderid: usize) -> Self {
        Fill {
            price,
            symbol,
            quantity,
            timestamp: SystemTime::now().duration_since(UNIX_EPOCH).unwrap(),
            orderid,
            fillid: get_fillid(),
        }
    }
}

#[derive(Clone, Debug)]
pub struct Panic;

#[derive(Clone, Debug)]
pub struct Quote {
    pub bid: Money,
    pub ask: Money,
    pub symbol: String,
    pub timestamp: Duration,
}

impl Quote {
    pub fn create(bid: Money, ask: Money, symbol: String) -> Self {
        Quote {
            bid,
            ask,
            symbol,
            timestamp: SystemTime::now().duration_since(UNIX_EPOCH).unwrap(),
        }
    }
}
