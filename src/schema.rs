use std::env::current_dir;
use std::fs::create_dir_all;
use std::fs::read_dir;
use std::fs::remove_file;
use std::fs::write;
use std::path::Path;

use schemars::schema::RootSchema;

pub use schemars::schema_for;

use super::messages::Quote;

fn generate_schema() {
    let mut out_dir = current_dir().unwrap();
    out_dir.push("schema");
    create_dir_all(&out_dir).unwrap();
    remove_schemas(&out_dir).unwrap();

    export_schema(&schema_for!(Quote), &out_dir);
}

pub fn export_schema(schema: &RootSchema, out_dir: &Path) {
    let title = schema
        .schema
        .metadata
        .as_ref()
        .map(|b| b.title.clone().unwrap_or_else(|| "untitled".to_string()))
        .unwrap_or_else(|| "unknown".to_string());
    write_schema(schema, out_dir, &title);
}

// use this if you want to override the auto-detected name of the object.
// very useful when creating an alias for a type-alias.
pub fn export_schema_with_title(schema: &RootSchema, out_dir: &Path, title: &str) {
    let mut schema = schema.clone();
    // set the title explicitly on the schema's metadata
    if let Some(metadata) = &mut schema.schema.metadata {
        metadata.title = Some(title.to_string());
    }
    write_schema(&schema, out_dir, &title);
}

/// Writes schema to file. Overwrites existing file.
/// Panics on any error writing out the schema.
fn write_schema(schema: &RootSchema, out_dir: &Path, title: &str) {
    // first, we set the title as we wish
    let path = out_dir.join(format!("{}.json", title));
    let json = serde_json::to_string_pretty(schema).unwrap();
    write(&path, json + "\n").unwrap();
    println!("Created {}", path.to_str().unwrap());
}

fn is_regular_file(path: &path::Path) -> Result<bool, io::Error> {
    Ok(path.symlink_metadata()?.is_file())
}

fn is_hidden(path: &Path) -> bool {
    match path.file_name() {
        Some(name) => name.to_os_string().to_string_lossy().starts_with('.'),
        None => false, // a path without filename is no .*
    }
}

fn is_json(path: &Path) -> bool {
    match path.file_name() {
        Some(name) => name.to_os_string().to_string_lossy().ends_with(".json"),
        None => false, // a path without filename is no *.json
    }
}

pub fn remove_schemas(schemas_dir: &Path) -> Result<(), io::Error> {
    let file_paths = read_dir(schemas_dir)?
        .filter_map(Result::ok) // skip read errors on entries
        .map(|entry| entry.path())
        .filter(|path| is_regular_file(path).unwrap_or(false)) // skip directories and symlinks
        .filter(|path| !is_hidden(path)) // skip hidden
        .filter(|path| is_json(path)) // skip non JSON
        ;

    for file_path in file_paths {
        println!("Removing {:?} …", file_path);
        remove_file(file_path)?;
    }
    Ok(())
}
