use std::collections::HashMap;
use std::fmt;

use super::messages::Fill;
use super::messages::Order;
use super::messages::Quote;

impl fmt::Display for Quote {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        fmt::Debug::fmt(self, f)
    }
}

impl fmt::Display for Fill {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        fmt::Debug::fmt(self, f)
    }
}

impl fmt::Display for Order {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        fmt::Debug::fmt(self, f)
    }
}

pub fn top_of_book() -> HashMap<String, Quote> {
    let top_of_book: HashMap<String, Quote> = HashMap::new();
    return top_of_book;
}
